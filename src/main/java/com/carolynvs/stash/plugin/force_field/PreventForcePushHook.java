package com.carolynvs.stash.plugin.force_field;

import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RefChangeType;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.setting.RepositorySettingsValidator;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.setting.SettingsValidationErrors;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PreventForcePushHook implements PreReceiveRepositoryHook, RepositorySettingsValidator
{
    private final CommitService commitService;
    private final I18nService i18nService;
    private final PathMatcher pathMatcher = new AntPathMatcher();

    public PreventForcePushHook(CommitService commitService, I18nService i18nService)
    {
        this.commitService = commitService;
        this.i18nService = i18nService;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse response)
    {
        List<String> protectedRefs = getProtectedRefs(context);
        Repository repository = context.getRepository();

        List<String> blockedRefs = identifyReferencesWithBlockedChangesets(context, refChanges, protectedRefs, repository);

        if (!blockedRefs.isEmpty())
        {
            printError(response, blockedRefs);
        }

        return blockedRefs.isEmpty();
    }

    private List<String> getProtectedRefs(@Nonnull RepositoryHookContext context)
    {
        ArrayList<String> protectedRefs = new ArrayList<String>();

        String[] refs = context.getSettings().getString("references").split(" ");
        for(String ref : refs)
        {
            ref = ReferenceCleaner.fixUnmatchableRegex(ref);
            protectedRefs.add(ref);
        }
        return protectedRefs;
    }

    private List<String> identifyReferencesWithBlockedChangesets(@Nonnull RepositoryHookContext context, Collection<RefChange> refChanges, List<String> protectedRefs, Repository repository)
    {
        List<String> blocked = new ArrayList<String>();
        boolean protectSelected = context.getSettings().getBoolean("protectSelected");
        for(RefChange refChange : refChanges)
        {
            String refId = refChange.getRefId();
            if(isProtectedRef(protectedRefs, refChange, protectSelected) && isForcePush(repository, refChange))
            {
                blocked.add(refId);
            }
        }
        return blocked;
    }

    private void printError(HookResponse response, List<String> blockedRefs)
    {
        response.out().println("=================================");
        response.out().println(i18nService.getText("force-field.plugin-name", "Force Field"));
        for (String refId : blockedRefs)
        {
            response.out().println(i18nService.getText("force-field.error-message", "The repository administrator has disabled force pushes to {0}", refId));
        }
        response.out().println("=================================");
    }

    private boolean isProtectedRef(List<String> restrictedRefs, RefChange refChange, final boolean protectSelected)
    {
        String refId = refChange.getRefId();
        for(String refPattern : restrictedRefs)
        {
            final boolean match = pathMatcher.match(refPattern, refId);
            if((protectSelected && match)
                    // if protectSelected is false, allow matching refs, block non-matching ones
                    ||(!protectSelected && !match))
            {
                return true;
            }
        }

        return false;
    }

    private boolean isForcePush(Repository repository, RefChange refChange)
    {
        boolean isReplacingChangesets = refChange.getType() == RefChangeType.UPDATE;
        if(!isReplacingChangesets)
            return false;

        Page<Changeset> changes = getChangesets(repository, refChange);
        return changes.getSize() > 0;
    }

    private Page<Changeset> getChangesets(Repository repository, RefChange refChange)
    {
        ChangesetsBetweenRequest request = new ChangesetsBetweenRequest.Builder(repository)
                .exclude(refChange.getToHash())
                .include(refChange.getFromHash())
                .build();

        return commitService.getChangesetsBetween(request, PageUtils.newRequest(0, 1));
    }

    /**
     * Validates the parent URL on the configuration form.
     * @param settings contains the values from the config form
     * @param errors validation errors need to be put into this object
     * @param repository the repo for which the hook is being configured
     */
    @Override
    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors errors, @Nonnull Repository repository)
    {
        final String refs = settings.getString("references");
        final Boolean protectSelected = settings.getBoolean("protectSelected");
        if (null == refs || refs.isEmpty())
            // this never appears for some reason, but the form cannot be saved if the field is empty
            errors.addFieldError("references", i18nService.getText("force-field.specify-branches-error", "Please specify branches."));
        if (null == protectSelected)
            // would be nice to set the default case of the radio button to one of the options,
            // instead of requiring the user to always make an explicit choice,
            // but I cannot figure out how to do that in the .soy file
            errors.addFieldError("protectSelected", i18nService.getText("force-field.select-protect-allow-error", "Please select a protect/allow option."));
    }
}
